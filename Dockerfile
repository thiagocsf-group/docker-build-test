FROM gophernet/traceroute

RUN traceroute -I -q 1 google.com
RUN traceroute -I -q 1 199.232.166.132
RUN traceroute -I -q 1 deb.debian.org
RUN traceroute -I -q 1 security.debian.org
RUN traceroute -I -q 1 rubygems.org
RUN traceroute -I -q 1 199.232.130.132
RUN traceroute -I -q 1 151.101.250.132
RUN traceroute -I -q 1 151.101.2.132 
RUN traceroute -I -q 1 151.101.66.132 
